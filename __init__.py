from st3m.application import Application, ApplicationContext
from st3m.ui.colours import PUSH_RED, GO_GREEN, BLACK
from st3m.goose import Dict, Any, Tuple
from st3m.input import InputState
from ctx import Context
import os


class Play(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self.count = 0
        self.folder = '/sd/ba'

        if not os.path.exists(self.folder):
            os.mkdir(self.folder)

            app_ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()
            app_ctx.text_align = ctx.CENTER
            app_ctx.text_baseline = ctx.MIDDLE
            app_ctx.font_size = 30
            app_ctx.rgb(1, 1, 1)
            app_ctx.move_to(0, 0)
            app_ctx.save()
            app_ctx.text("no images on SD")
            app_ctx.restore()

        self.files = os.listdir(self.folder)
        
    def draw(self, ctx: Context) -> None:
        # Paint the background black
        # ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()

        image = "{}/{}".format(self.folder, self.files[self.count])
        ctx.image(image, -120, -120, 240, 240)
        self.count += 1

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        if self.count == len(self.files):
            self.count = 0



# For running with `mpremote run`:
if __name__ == "__main__":
    import st3m.run

    st3m.run.run_view(Play(ApplicationContext()))